@extends('layout')

@section('content')

    <form method="POST" action="/news/create">
        @csrf
        <div class="form-group">
            <label class="col-md-4 control-label">Тема</label>
            <div class="col-md-6">
                <input type="text" class="form-control" name="theme" placeholder="тема новости">
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-4 control-label">Название</label>
            <div class="col-md-6">
                <input type="text" class="form-control" name="title" placeholder="Название">
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-4 control-label">Содержание</label>
            <div class="col-md-6">
                <input type="text" class="form-control" name="text" placeholder="Содержание">
            </div>
        </div>

        <div class="form-group">
            <div class="col-md-6 col-md-offset-4">
                <button type="submit" class="btn btn-primary" style="margin-right: 15px;">
                    Создать
                </button>
            </div>
        </div>

    </form>

@endsection