@extends('layout')

@section('content')
    <a href="/show__news_create_form">Создать новость</a>

    @foreach ($news as $newsItem)

        <div class="card">
            <div class="card-header">
                {{ $newsItem->theme }}
            </div>
            <div class="card-body">
                <h5 class="card-title">
                    {{ $newsItem->title }}
                </h5>
                <p class="card-text">
                    {{ $newsItem->text }}
                </p>
            </div>
        </div>

    @endforeach

@endsection