@extends('layout')

@section('content')

    <section class="jumbotron text-center">
        <div class="container">
            <h1 class="jumbotron-heading">{{$tour->name}}</h1>
        </div>
    </section>

    <div class="album py-5 bg-light">
        <div class="container">

            <div class="row">
                <div class="col-md-8">
                    <div class="card mb-8 box-shadow">
                        <img class="card-img-top" src="{{$tour->image}}" alt="Card image cap">
                        <div class="card-body">
                            <p class="card-text">{{$tour->name}}</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card mb-4 box-shadow">
                        <div class="card-body">
                            <p class="card-text">{{$tour->description}}</p>
                            <div class="d-flex justify-content-between align-items-center">
                                <small class="text-muted">{{$tour->cost}}</small>
                                <small class="text-muted">{{$tour->country->name}}</small>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <br>

            <div class="row">
                <div class="col-md-12">
                    <h1 class="jumbotron-heading">Комментарии</h1>
                </div>
                @foreach($tour->comments as $comment)
                <div class="col-md-12">
                    <div class="card mb-12 box-shadow">
                        <div class="card-body">
                            <p class="card-text">
                                {{$comment->text}}
                            </p>
                            <div class="d-flex justify-content-between align-items-center">
                                <small class="text-muted">{{$comment->user->name}}</small>
                                @if(Auth::user()->id == $comment->user->id)
                                <a href="/comment/delete/{{$comment->id}}">Удалить</a>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
            <br>
            <form method="POST" action="/comment/create">
                @csrf
                <input type="hidden" name="tour_id" value="{{$tour->id}}">
                <div class="row">
                    <div class="col-md-12">
                        <textarea class="form-control" name="text" placeholder="текст комментария">
                            {{old('text')}}
                        </textarea>
                    </div>
                    @if ($errors->has('text'))
                        <div class="form_field_error">
                            {{ $errors->first('text') }}
                        </div>
                    @endif
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <button type="submit" class="btn btn-primary" style="margin-right: 15px;">
                            Создать
                        </button>
                    </div>
                </div>

            </form>

        </div>
    </div>

@endsection