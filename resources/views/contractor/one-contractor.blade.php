@extends('layout')

@section('content')
    <section class="jumbotron text-center">
        <div class="container">
            <h1 class="jumbotron-heading">{{$contractor->name}}</h1>
            <p class="lead text-muted">{{$contractor->full_name}}</p>
            <p>
                <small class="text-muted">{{$contractor->address}}</small>
                <small class="text-muted">{{$contractor->website}}</small>
            </p>
        </div>
    </section>

    <div class="album py-5 bg-light">
        <div class="container">

            <div class="row">
                @foreach($contractor->tours as $tour)
                <div class="col-md-4">
                    <div class="card mb-4 box-shadow">
                        <img class="card-img-top" src="{{$tour->image}}" alt="Card image cap">
                        <div class="card-body">
                            <p class="card-text">{{$tour->name}}</p>
                            <div class="d-flex justify-content-between align-items-center">
                                <div class="btn-group">
                                    <a href="{{url('tour/'.$tour->id)}}"><button type="button" class="btn btn-sm btn-outline-secondary">Подробнее</button></a>
                                </div>
                                <small class="text-muted">{{$tour->cost}}</small>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
@endsection