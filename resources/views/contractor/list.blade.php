@extends('layout')

@section('content')
    <section class="jumbotron text-center">
        <div class="container">
            <h1 class="jumbotron-heading">Турфирмы</h1>
        </div>
    </section>

    <div class="album py-5 bg-light">
        <div class="container">

            <div class="row">
                @foreach($contractors as $contractor)
                <div class="col-md-4">
                    <div class="card mb-4 box-shadow">
                        <div class="card-body">
                            <p>{{$contractor->name}}</p>
                            <p class="card-text">{{$contractor->full_name}}</p>
                            <div class="d-flex justify-content-between align-items-center">
                                <div class="btn-group">
                                    <a href="{{url('contractor/'.$contractor->id)}}"><button type="button" class="btn btn-sm btn-outline-secondary">Подробнее</button></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach

                </div>

            </div>
        </div>
    </div>

@endsection