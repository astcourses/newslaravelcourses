@extends('layout')

@section('content')
    <div class="row">
        <div class="col-4">
            <img src="/public/images/profile.jpg" class="img-fluid img-profile" alt="Responsive image">
        </div>
        <div class="col-8">
            <p><b>Полное имя: </b>
                {{ $user->name }}
            </p>
            <p>
                <b>О себе: </b>
                {{ $user->disc }}
            </p>
            <p>
                <span class="contact-info">тел.
                {{ $user->number }}
                </span>
            </p>
        </div>
    </div>

@endsection