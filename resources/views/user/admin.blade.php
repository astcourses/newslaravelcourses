@extends('layout-admin')

@section('content')
    <table class="table">
        <thead>
        <tr>
            <th>#</th>
            <th>Название</th>
            <th>Стоимость</th>
            <th>Страна</th>
            <th>Действия</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <th scope="row">1</th>
            <td>Домбай. Рождество. 2 дня.</td>
            <td>7200 р.</td>
            <td>Россия</td>
            <td><i class="fas fa-pencil-alt cst-icon"></i><i class="fas fa-trash-alt cst-icon"></i></td>
        </tr>
        <tr>
            <th scope="row">2</th>
            <td>Краснодар + Горячий ключ</td>
            <td>8500 р.</td>
            <td>Россия</td>
            <td><i class="fas fa-pencil-alt cst-icon"></i><i class="fas fa-trash-alt cst-icon"></i></td>
        </tr>
        <tr>
            <th scope="row">3</th>
            <td>Адыгея. Рождество.</td>
            <td>9100 р.</td>
            <td>Россия</td>
            <td><i class="fas fa-pencil-alt cst-icon"></i><i class="fas fa-trash-alt cst-icon"></i></td>
        </tr>
        </tbody>
    </table>
@endsection