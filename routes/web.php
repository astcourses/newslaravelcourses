<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;

Route::get('/', 'UserController@showProfile')->middleware('check_role:user');

Auth::routes();

Route::get('/contractors', 'ContractorsController@allContractors');
Route::get('/contractor/{id}', 'ContractorsController@getContractor');
Route::get('/tour/{id}', 'ToursController@getTour');
Route::get('/news', 'NewsController@getAll');
Route::post('/news/create', 'NewsController@create');
Route::get('/show__news_create_form', 'NewsController@getCreateView');
Route::post('/comment/create', 'CommentsController@create');
Route::get('/comment/delete/{id}', 'CommentsController@delete');

Route::get('/admin', 'UserController@showAdmin')->middleware(['auth', 'check_role:admin_tour']);
