<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateToursTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tours', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 256);
            $table->text('description');
            $table->integer('contractor_id')->unsigned()->nullable();
            $table->integer('cost');
            $table->integer('country_id')->unsigned()->nullable();
            $table->string('image', 256);
            $table->timestamps();

            $table->foreign('contractor_id')->references('id')->on('contractors');
            $table->foreign('country_id')->references('id')->on('countries');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tours');
    }
}
