<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contractor extends Model
{
    public function tours()
    {
        return $this->hasMany('App\Tour');
    }
}
