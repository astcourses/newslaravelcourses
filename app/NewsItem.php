<?php

namespace App;


use Illuminate\Database\Eloquent\Model;

class NewsItem extends Model
{
    protected $table = 'news';

    protected $fillable = [
        'theme', 'title', 'text'
    ];

    public $timestamps = false;

}