<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class CheckRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $role)
    {
        if (!Auth::user()) {
            return redirect('/login');
        }
        if ($role != Auth::user()->role) {
            $url = (Auth::user()->role == 'admin_tour') ? '/admin': '/';
            return redirect($url);
        }

        return $next($request);
    }
}
