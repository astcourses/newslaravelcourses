<?php

namespace App\Http\Controllers;


use App\User;
use Auth;

class UserController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * Показать профиль пользователя
     */
    public function showProfile()
    {
        $user = User::find(1);

        return view('user.profile')->with('user', $user);
    }

    public function showAdmin()
    {
        $tours = Auth::user()->contractor->tours;
        dd( $tours );
        return view('user.admin', [
            'tours' => $tours
        ]);
    }

}