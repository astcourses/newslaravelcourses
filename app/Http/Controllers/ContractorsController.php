<?php

namespace App\Http\Controllers;

use App\Contractor;

class ContractorsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function allContractors()
    {
        $contractors = Contractor::all();
        return view('contractor.list', ['contractors' => $contractors]);
    }

    public function getContractor($id)
    {
        $contractor = Contractor::find($id);
        return view('contractor.one-contractor', ['contractor' => $contractor]);
    }

}