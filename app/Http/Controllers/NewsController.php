<?php

namespace App\Http\Controllers;


use App\NewsItem;
use Illuminate\Http\Request;

class NewsController
{

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * Получить все новости
     */
    public function getAll()
    {
        $news = NewsItem::all();

        return view('news.all')->with('news', $news);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * Получить форму для создания новости
     */
    public function getCreateView()
    {
        return view('news.create');
    }

    /**
     * @param Request $requets
     * Создать новость
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create(Request $request)
    {
        $newNewsItem = new NewsItem();
        $newNewsItem->theme = $request['theme'];
        $newNewsItem->title = $request['title'];
        $newNewsItem->text = $request['text'];

        $newNewsItem->save();

        $news = NewsItem::all();
        return view('news.all')->with('news', $news);
    }
}