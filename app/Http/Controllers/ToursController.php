<?php

namespace App\Http\Controllers;

use App\Tour;

class ToursController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getTour($id)
    {
        $tour = Tour::find($id);
        return view('tour.one-tour', ['tour' => $tour]);
    }

}