<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Comment;
use Auth;
use App\Http\Requests\CommentRequest;

class CommentsController extends Controller
{
    public function create(CommentRequest $request) {
        $comment = new Comment();
        $comment->text = $request->text;
        $comment->user_id = Auth::user()->id;
        $comment->tour_id = $request->tour_id;
        $comment->save();
        return redirect()->back();
    }

    public function delete($id) {
        $comment = Comment::find($id);
        if ($comment->user->id == Auth::user()->id) {
            $comment->delete();
        }
        return redirect()->back();
    }
}
