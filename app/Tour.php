<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tour extends Model
{
    public function country()
    {
        return $this->belongsTo('App\Country');
    }

    public function comments()
    {
        return $this->hasMany('App\Comment');
    }

}
